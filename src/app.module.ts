import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import { RESOLVERS } from './resolvers';

import { Database } from 'arangojs';

@Module({
  imports: [
    GraphQLModule.forRoot({
      debug: false,
      playground: true,
      autoSchemaFile: true,
    }),
  ],
  controllers: [AppController],
  providers: [
    AppService,
    {
      provide: Database,
      async useFactory() {
        const result = new Database({
          url: 'http://localhost:8529',
        });

        result.useDatabase('Galdr');
        result.useBasicAuth('root', 'changeit');

        return result;
      },
    },
    ...RESOLVERS,
  ],
})
export class AppModule {}
