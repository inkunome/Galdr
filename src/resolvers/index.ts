import { GameResolver } from './game.resolver';
import { PlayerResolver } from './player.resolver';
import { PlaygroundResolver } from './playground.resolver';

export const RESOLVERS = [GameResolver, PlayerResolver, PlaygroundResolver];
