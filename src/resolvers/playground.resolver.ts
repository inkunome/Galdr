import {
  Int,
  Resolver,
  Query,
  Args,
  ResolveField,
  Parent,
  Mutation,
} from '@nestjs/graphql';
import { aql, Database, DocumentCollection, EdgeCollection } from 'arangojs';
import { Document } from 'arangojs/lib/cjs/util/types';

import { Playground } from '../models';

import { GameResolver } from './game.resolver';
import { PlayerResolver } from './player.resolver';

@Resolver((of) => Playground)
export class PlaygroundResolver {
  constructor(
    private readonly database: Database,
    private readonly games: GameResolver,
    private readonly players: PlayerResolver,
  ) {
    this.playgrounds = database.collection('Playground');
    this.plays = database.edgeCollection('plays');
  }

  @Query((returns) => Playground)
  async playground(
    @Args('id', { type: () => Int }) id: string,
  ): Promise<Document<Playground>> {
    return this.playgrounds.document({ _key: id });
  }

  @Mutation((returns) => Playground)
  async createPlayground(
    @Args('name', { type: () => String }) name: string,
    @Args('game', { type: () => String }) gameId: string,
  ) {
    const game = await this.games.game(gameId);

    if (game) {
      return (
        await this.playgrounds.save(
          { name, game, players: [] },
          { returnNew: true },
        )
      ).new;
    }
  }

  @Mutation((returns) => Playground)
  async addPlayerToPlayground(
    @Args('id', { type: () => String }) id: string,
    @Args('player', { type: () => String }) playerId: string,
  ) {
    const playground = await this.playground(id);
    const player = await this.players.player(playerId);

    if (playground && player) {
      await this.database.query(aql`
        UPSERT { _from: ${playground._id}, _to: ${player._id} } 
        INSERT { _from: ${playground._id}, _to: ${player._id} } 
        UPDATE { } IN ${this.plays}
      `);
    }

    return playground;
  }

  private readonly playgrounds: DocumentCollection<Playground>;
  private readonly plays: EdgeCollection;
}
