import {
  Int,
  Resolver,
  Query,
  Args,
  ResolveField,
  Parent,
  Mutation,
} from '@nestjs/graphql';
import { aql, Database, DocumentCollection } from 'arangojs';

import { Game } from '../models';

@Resolver((of) => Game)
export class GameResolver {
  constructor(private readonly database: Database) {
    this.games = database.collection('Game');
  }

  @Query((returns) => Game)
  async game(@Args('id', { type: () => Int }) id: string): Promise<Game> {
    return this.games.document({ _key: id });
  }

  @Mutation((returns) => Game)
  async createGame(
    @Args('name', { type: () => String }) name: string,
  ): Promise<Game> {
    return (await this.games.save({ name }, { returnNew: true })).new;
  }

  private readonly games: DocumentCollection<Game>;
}
