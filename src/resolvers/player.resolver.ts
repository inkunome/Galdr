import {
  Int,
  Resolver,
  Query,
  Args,
  ResolveField,
  Parent,
  Mutation,
} from '@nestjs/graphql';
import { aql, Database, DocumentCollection } from 'arangojs';
import { Document } from 'arangojs/lib/cjs/util/types';

import { Player } from '../models';

@Resolver((of) => Player)
export class PlayerResolver {
  constructor(private readonly database: Database) {
    this.players = database.collection('Player');
  }

  @Query((returns) => Player)
  async player(
    @Args('id', { type: () => Int }) id: string,
  ): Promise<Document<Player>> {
    return this.players.document({ _key: id });
  }

  @Mutation((returns) => Player)
  async createPlayer(
    @Args('name', { type: () => String }) name: string,
  ): Promise<Player> {
    return (await this.players.save({ name }, { returnNew: true })).new;
  }

  private readonly players: DocumentCollection<Player>;
}
