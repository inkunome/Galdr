import { Field, ObjectType } from '@nestjs/graphql';

import { Game, Player } from './index';

@ObjectType()
export class Playground {
  @Field((type) => String)
  id?: string;

  @Field()
  name: string;

  @Field((type) => Game)
  game: Game;

  @Field((type) => [Player])
  players: Player[];
}
