import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class Game {
  @Field((type) => String)
  id?: string;

  @Field({ nullable: true })
  name?: string;
}
