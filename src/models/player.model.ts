import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class Player {
  @Field((type) => String)
  id?: string;

  @Field()
  name: string;
}
